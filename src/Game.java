import  java.awt.*;
import  java.awt.event.*;
import  java.util.*;
import  javax.swing.*;

public class Game implements ActionListener{
    // Vars
    Random random = new Random();
    JFrame frame = new JFrame();
    JPanel title_panel = new JPanel();
    JPanel button_panel = new JPanel();
    JLabel textfield = new JLabel();
    // aray of the buttons
    JButton[] buttons = new JButton[9];
    boolean player1_turn;

    Game(){ // constructor
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(700,700);
        frame.getContentPane().setBackground(new Color(50,50,50));
        frame.setLayout(new BorderLayout());
        frame.setVisible(true);
        textfield.setBackground(new Color(25,25,25));
        textfield.setForeground(new Color(255,255,255));
        textfield.setFont(new Font("JetBrainsMono Nerd Font",Font.PLAIN,75));
        textfield.setHorizontalAlignment(JLabel.CENTER);
        textfield.setText("Tic-Tac-Toe");
        textfield.setOpaque(true);
        title_panel.setLayout(new BorderLayout());
        title_panel.setBounds(0,0,700,100);
        title_panel.add(textfield);
        frame.add(title_panel,BorderLayout.NORTH);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }

    public void FirstTurn(){

    }
    public void Check(){

    }
    public void XWins(int a, int b,int c){

    }
    public void OWins(int a, int b,int c){

    }
}
